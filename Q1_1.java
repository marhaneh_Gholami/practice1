import java.util.Scanner;

public class Q1_1 {
	static Scanner sc=new Scanner(System.in);
	public static void main(String[] args) {
		
		int number=sc.nextInt();
		int[][] diamond=new int[2*number-1][2*number-1];
		for(int i=0;i<2*number-1;i++) {
			if (i<number) {
				for(int j=number-1-i;j<=number-1+i;j++) {
					diamond[j][i]=number-i;
				}
			}
				else {
					for(int j=i-number+1;j<=3*number-i-3;j++) {
					diamond[j][i]=i-number+2;
					}
				}
		}
		for(int m=0;m<2*number-1;m++) {
			for(int n=0;n<2*number-1;n++) {
				if (diamond[m][n]!=0)
					System.out.print(diamond[m][n]);
				else
					System.out.print(" ");
			}
		System.out.println();
		}
	}
	
	
}
